package com.hhf.forum.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MyFavorite {
    private int status;
}
