DROP DATABASE IF EXISTS forum;
CREATE DATABASE IF NOT EXISTS forum;
USE forum;

-- 论坛帖子
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `user_id` int NOT NULL,
                        `title` varchar(255) NOT NULL,
                        `content` varchar(255) DEFAULT NULL,
                        `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `favorite_count` int DEFAULT NULL,
                        `view_count` int DEFAULT NULL,
                        `reply_count` int DEFAULT NULL,
                        `status_host` int DEFAULT NULL COMMENT '0有效帖子  1被管理员删除',
                        `status_guest` int DEFAULT NULL COMMENT '0有效帖子   1被发帖人自己删除的帖子',
                        `deleted_at` datetime DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5136 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `myfavorite`;
CREATE TABLE `myfavorite`  (
                               `id` int NOT NULL AUTO_INCREMENT,
                               `user_id` int NOT NULL,
                               `post_id` int NOT NULL,
                               `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                               `status` int NULL DEFAULT NULL COMMENT '1 收藏  0 取消收藏',
                               `updated_at` datetime NULL DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `avatar`;
CREATE TABLE `avatar` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `avatar` varchar(255) NOT NULL,
                          `created_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `reply4post`;
CREATE TABLE `reply4post` (
                              `id` int NOT NULL AUTO_INCREMENT,
                              `post_id` int NOT NULL,
                              `reply_content` varchar(255) NOT NULL,
                              `user_id` int NOT NULL,
                              `reply_favorite_count` int DEFAULT NULL,
                              `reply_reply_count` int DEFAULT NULL,
                              `created_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `reply4reply`;
CREATE TABLE `reply4reply` (
                               `id` int NOT NULL AUTO_INCREMENT,
                               `from_user_id` int NOT NULL,
                               `reply_id` int NOT NULL,
                               `reply4reply_content` varchar(255) NOT NULL,
                               `to_user_id` int NOT NULL,
                               `post_id` int DEFAULT NULL,
                               `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                               `r4r_favorite_count` int DEFAULT NULL,
                               `r4r_reply_count` int DEFAULT NULL,
                               `from_nickname` varchar(255) DEFAULT NULL,
                               `to_nickname` varchar(255) DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
                         `id` int NOT NULL AUTO_INCREMENT,
                         `user_id` int NOT NULL,
                         `token` varchar(255) NOT NULL,
                         `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `username` varchar(255) NOT NULL,
                        `nickname` varchar(255) NOT NULL,
                        `password` varchar(255) NOT NULL,
                        `avatar_id` int NOT NULL,
                        `mail` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                        `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        `updated_at` datetime DEFAULT NULL,
                        `level` int DEFAULT NULL,
                        `status` int NOT NULL COMMENT '0 普通用户 1 版主  2超级管理员',
                        `logintime` datetime DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;